import sys, pytest
sys.path.append(sys.path[0] + '/..')

from linkedlists.singly_linked_list import Node, SinglyLinkedList

@pytest.fixture
def create_node():
    return Node(1)

@pytest.fixture
def create_sll():
    return SinglyLinkedList(1)

@pytest.fixture
def create_populated_sll(create_sll):
    sll = create_sll
    for x in range(2, 11):
        sll.insert(x)
    return sll

class TestSinglyLinkedList:

    def test_create_connected_nodes(self):
        x = Node(1)
        y = Node(2)
        z = Node(3)
        x.nextNode = y
        y.nextNode = z
        assert x.nextNode.nodeValue == 2
        assert y.nextNode.nodeValue == 3

    def test_create_sll(self, create_sll):
        assert create_sll.head != None

    def test_create_sll_add_head(self, create_sll):
        assert create_sll.head.nodeValue == 1

    def test_create_sll_insert_node(self, create_sll):
        create_sll.insert(2)
        assert create_sll.head.nextNode.nodeValue == 2

    def test_create_sll_fixture(self, create_populated_sll):
        assert create_populated_sll.listToString() == "1\t2\t3\t4\t5\t6\t7\t8\t9\t10\t"

    def test_search_sll(self, create_populated_sll):
        x = create_populated_sll.search(4)
        y = create_populated_sll.search(44)
        create_populated_sll.insert(103)
        z = create_populated_sll.search(103)
        assert x.nodeValue == 4
        assert y == None
        assert z != None
        assert z.nodeValue == 103

    def test_insert_duplicate(self, create_populated_sll):
        assert create_populated_sll.insert(2) == False
        assert create_populated_sll.insert(1) == False
        create_populated_sll.insert(10)
        print(create_populated_sll.listToString())
        assert create_populated_sll.listToString() == "1\t2\t3\t4\t5\t6\t7\t8\t9\t10\t"