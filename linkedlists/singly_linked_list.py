# singly linked list data structure
class Node:
    def __init__(self, nodeValue):
        self.nodeValue = nodeValue
        self.nextNode = None

class SinglyLinkedList:
    def __init__(self, headNodeValue=None):
        self.head = Node(headNodeValue)

    def insert(self, nodeValue):
        """
        Given an integer value,
        insert a new node at the end of the list.
        Should not be successful if a node with this value exists.
        """
        if self.head == None:
            self.head = Node(nodeValue)
            return True
        else:
            x = self.head
            while x.nextNode != None and x.nodeValue != nodeValue:
                x = x.nextNode

            if x.nodeValue == nodeValue:
                print("Duplicate node already exists.")
                return False
            else:
                x.nextNode = Node(nodeValue)
                return True


    def search(self, searchValue):
        """
        Given an integer value, we search for the node within the linked list that has this value.
        If it does not exist, return a null value.
        If it does, return the node in question.
        """
        if self.head == None:
            print("ERROR: List is empty.")
            return None
        else:
            if self.head.nodeValue == searchValue:
                return self.head
            else:
                x = self.head
                while x.nextNode != None and x.nodeValue != searchValue:
                    x = x.nextNode

                if x.nodeValue == searchValue:
                    return x
                else:
                    return None


    def listToString(self):
        x = self.head
        listToString = ""

        if x == None:
            return "List is empty."
        else:
            while x != None:
                listToString += (str(x.nodeValue) + "\t")
                x = x.nextNode

        return listToString